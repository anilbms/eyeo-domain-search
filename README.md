# Eyeo Javascript Developer assignment
## Base Domain Finder 
This application consists of 2 classes 
	1. *BaseDomainFinder* 
	2. *PublicDomain*


### *BaseDomainFinder*
*BaseDomainFinder* has method to search baseDomain for given subDomain using *PublicDomain*

### *PublicDomain*
*PublicDomain* has methods to generate random domains and adding a new domain to domains list. Random public domains are generated for testing purpose.


###Folder structure 
eyeo-domain-search
	src/js/
	specs/

### Test application 
After cloning the repository, perform *npm update*.
Unit tests are written using [Jasmine](http://jasmine.github.io/).
To run test perform *npm test*
